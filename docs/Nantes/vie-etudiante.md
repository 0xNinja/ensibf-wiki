# 🎉 Vie étudiante

## Sortir

### Bars 🍻

Nos bars Nantais ont du talent. N'hésitez pas à leur rendre visite :

- [Game Over](https://g.page/gameover_nantes) / Esport
- [Barberousse Nantes](https://g.page/barberousse-nantes) / Bar à rhums
- [Les Fleurs du Malt](https://goo.gl/maps/RFZbNRj8BgckZ2ss7) / Bar à bières

### Boites de nuit 🌇

- [Warehouse](https://goo.gl/maps/2ZHSQR5AFZudy2BD7)
- [New Factory](https://goo.gl/maps/1zLZmtudisDVk6rC9)
