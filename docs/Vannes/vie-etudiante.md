# 🎉 Vie étudiante

## Sortir

### Bars 🍻

Nos bars Vannetais ont du talent. N'hésitez pas à leur rendre visite :

- [La boussole](https://goo.gl/maps/AWhBGq37osAnM6Ja6)
- [Les Valseuses](https://goo.gl/maps/zu3WUaPuEuFF1zTY9) / Fléchettes
- [Le Paddy](https://goo.gl/maps/tygo2eH2PPAKDgze7)
- [Brasserie Awen](https://www.brasserie-awen.bzh/)
- [DéDaLe](https://goo.gl/maps/S78sYb5Acg7DSwLe7)
- [Warpzone - Vannes](https://g.page/WpZ_Vannes?share) / Fléchettes / Esport

!!! tip "La boussole 🧭"
    La boussole est le bar vannetais partenaire du BDE et offre des réductions si vous êtes adhérent en montrant votre bracelet

### Boite de nuit 🌇

- [Bubble](https://goo.gl/maps/WXL1kHvJoCsCTufZA)

### Parkings 🚗

- [En bas des remparts](https://goo.gl/maps/rsJxDxZQNCkWVzc1A)
- [Capucins](https://goo.gl/maps/w7hhajv2WKpXonGY6)
- [Cimetière de Calmont](https://goo.gl/maps/J8wWS5xvVJFgV8yi8)
