# 💰 Argent

Par ici la monnaie. Comme tout étudiant qui se respecte tu cherches de la moula et c'est bien normal. La France regorge d'argent disponible pour aider les étudiants à débuter leur vie, donc ne passez pas à côté d'un petit coup de pouce.

## Grille minimum alternant 2021

Grille des salaires minimums bruts avec le pourcentage du SMIC.

| Année du contrat | Salaire < 18ans | Salaire 18-20     | Salaire 21-25     | Salaire >= 26      |
| ---------------- | --------------- | ----------------- | ----------------- | ------------------ |
| 1ère             | 419,74 € / 27%  | 668,47 € / 43%    | 823,93€ / 53%     | 1 554,58 € / 100%  |
| 2ème             | 606,29 € / 39%  | 792,84 € / 51%    | 948,29 € / 61%    | 1 554,58 € / 100%  |
| 3ème             | 855,02 € / 55%  | 1 041,57 € / 78%  | 1.212,57 € / 78%  | 1 554,58 € / 100%  |

!!! success
    Les salaires des alternants **bruts** sont presque équivalents aux salaires **net**. La différence n'est que de quelques euros.

## CVEC

Chaque année, vous serez soumis à la [CVEC](https://cvec.etudiant.gouv.fr/) (Contribution à la vie étudiante) comme frais d'accès au campus. Cette dernière vous coûtera la bagatelle de 92 € (Tarif 2020/2021). Elle est obligatoire et peut vous être remboursé si vous êtes boursier.

## CE

Si votre entreprise d'accueil possède un CE, renseignez-vous dès votre arrivée. Les CE peuvent proposer de nombreux avantages (Chèques vacances, Chèque culture, Ticket cinéma, location, etc).

## Logement

Voici une liste d'aides spécifiques au logement :

- [APL](https://www.service-public.fr/particuliers/vosdroits/F12006) l'aide au logement de la CAF
- [Mobili Jeune - Action Logement](https://www.actionlogement.fr/l-aide-mobili-jeune) l'aide du groupement action logement pour les alternants de moins de 30ans

## Non classé

- [Prime d'activité](https://www.service-public.fr/particuliers/vosdroits/N31477) prime pour les actifs. Accordée aux alternants touchants + de 78% du smic
- [Aide à la mobilité](https://www.actionlogement.fr/aide-mobilite) l'aide du groupement action logement lors d'un déménagement pour vous rapprocher de votre lieu de travail (Alternance)
